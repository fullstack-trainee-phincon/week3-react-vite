import "./App.css";
import Facebook from "./assets/images/icon-facebook.svg";
import Pinterest from "./assets/images/icon-pinterest.svg";
import Instagram from "./assets/images/icon-instagram.svg";
import React from "react";
import Countdown from "react-countdown";

const App = () => {
  const date = Date.now() + 365 * 24 * 60 * 60 * 1000;

  const renderer = ({ days, hours, minutes, seconds }) => {
    const flipSeconds = seconds > 0 ? "flip-in-top" : "flip-out-top";

    return (
      <div className="Countdown-Box">
        <div className="Countdown-Timer">
          <div className="CountdownUp" />
          <div className="CountdownDown" />
          <div className="TimerDays">{days}</div>
          <div className="Label">Days</div>
        </div>
        <div className="Countdown-Timer">
          <div className="CountdownUp" />
          <div className="CountdownDown" />
          <div className="Timer">{hours}</div>
          <div className="Label">Hours</div>
        </div>
        <div className="Countdown-Timer">
          <div className="CountdownUp" />
          <div className="CountdownDown" />
          <div className="Timer">{minutes}</div>
          <div className="Label">Minutes</div>
        </div>
        <div className="Countdown-Timer">
          <div className="CountdownUp" />
          <div className="CountdownDown" />
          <div className="Timer">{seconds}</div>
          <div className="Label">Seconds</div>
        </div>
      </div>
    );
  };

  return (
    <div className="Container-Main">
      <div className="Text1">We are launching soon</div>
      <Countdown date={date} renderer={renderer} />
      <div className="Footer">
        <div className="Social-Footer">
          <img src={Facebook} alt="Facebook" />
          <img src={Pinterest} alt="Pinterest" />
          <img src={Instagram} alt="Instagram" />
        </div>
      </div>
    </div>
  );
};

export default App;
