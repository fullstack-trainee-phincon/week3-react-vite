import React from "react";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";

export default function MediaCard({ countryData }) {
  return (
    <Card sx={{ maxWidth: 300 }}>
      {countryData && (
        <CardMedia
          sx={{ height: 140 }}
          image={countryData.flags.svg}
          title={countryData.name.common}
        ></CardMedia>
      )}
      <Card />

      <CardContent>
        {countryData && (
          <>
            <Typography gutterBottom variant="h5" component="div">
              {countryData.name.common}
            </Typography>
            <Typography variant="body2" color="text.secondary">
              Population: {countryData.population}
            </Typography>
            <Typography variant="body2" color="text.secondary">
              Region: {countryData.region}
            </Typography>
            <Typography variant="body2" color="text.secondary">
              Capital: {countryData.capital}
            </Typography>
          </>
        )}
      </CardContent>
    </Card>
  );
}
