// SearchBar.jsx
import React from "react";
import IconButton from "@mui/material/IconButton";
import SearchIcon from "@mui/icons-material/Search";
import TextField from "@mui/material/TextField";

const SearchBar = ({ setSearchQuery }) => (
  <form>
    <TextField
      sx={{ minWidth: 200, marginTop: 5 }}
      id="search-bar"
      className="text"
      onInput={(e) => {
        setSearchQuery(e.target.value);
      }}
      label="Search by Country"
      variant="outlined"
      placeholder="Search..."
      size="small"
    />
    <IconButton sx={{ marginTop: 5 }} type="submit" aria-label="search">
      <SearchIcon style={{ fill: "hsl(209, 23%, 22%)" }} />
    </IconButton>
  </form>
);

export default SearchBar;
