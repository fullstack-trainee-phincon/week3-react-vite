import React, { useState, useEffect } from "react";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import Navbar from "./components/Navbar.jsx";
import Filter from "./components/Filter";
import MediaCard from "./components/Card";
import axios from "axios";
import SearchBar from "./components/SearchBar.jsx";
import classes from "../src/App.module.scss";

const lightTheme = createTheme({
  palette: {
    primary: {
      main: "#606060",
    },
    secondary: {
      main: "#dedede",
    },
    mode: "light",
  },
});

const darkTheme = createTheme({
  palette: {
    primary: {
      main: "#606060",
    },
    secondary: {
      main: "#dedede",
    },
    mode: "dark",
  },
});

const App = () => {
  const [countriesData, setCountriesData] = useState([]);
  const [darkMode, setDarkMode] = useState(false);
  const [selectedRegion, setSelectedRegion] = useState("All");
  const [searchQuery, setSearchQuery] = useState("");

  useEffect(() => {
    axios
      .get("https://restcountries.com/v3.1/all")
      .then((response) => {
        setCountriesData(response.data);
      })
      .catch((error) => {
        console.error("Error fetching country data:", error);
      });
  }, []);

  useEffect(() => {
    document.body.classList.toggle("dark-mode", darkMode);
  }, [darkMode]);

  const handleThemeToggle = () => {
    setDarkMode((prevDarkMode) => !prevDarkMode);
  };

  const filterData = (query, data) => {
    if (!query) {
      return data;
    } else {
      return data.filter((d) =>
        d.name.common.toLowerCase().includes(query.toLowerCase())
      );
    }
  };

  const handleFilterChange = (event) => {
    setSelectedRegion(event.target.value);
  };

  const dataFiltered = filterData(searchQuery, countriesData);

  return (
    <ThemeProvider theme={darkMode ? darkTheme : lightTheme}>
      <Navbar darkMode={darkMode} handleThemeToggle={handleThemeToggle} />
      <div className={classes.Box2}>
        <div className={classes.Box2of2}>
          <SearchBar setSearchQuery={setSearchQuery} />
          <Filter
            selectedRegion={selectedRegion}
            handleFilterChange={handleFilterChange}
          />
        </div>
      </div>
      <div className={classes.content1}>
        <div
          className={`${classes.Content} ${
            darkMode ? classes.darkModeContent : ""
          }`}
        >
          {dataFiltered
            .filter(
              (country) =>
                selectedRegion === "All" || country.region === selectedRegion
            )
            .map((country, index) => (
              <MediaCard key={index} countryData={country} />
            ))}
        </div>
      </div>
    </ThemeProvider>
  );
};

export default App;
