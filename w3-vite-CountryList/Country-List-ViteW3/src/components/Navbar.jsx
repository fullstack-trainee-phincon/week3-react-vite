import React from "react";
import classes from "../assets/style/NavBar.module.scss";
import Button from "@mui/material/Button";
import Brightness4Icon from "@mui/icons-material/Brightness4";
import Brightness7Icon from "@mui/icons-material/Brightness7";

const Navbar = ({ darkMode, handleThemeToggle }) => {
  return (
    <div className={`${classes.NavBox} ${darkMode ? classes.darkMode : ""}`}>
      <div
        className={`${classes.Text} ${darkMode ? classes.darkModeText : ""}`}
      >
        Where in the world?
      </div>

      <div className={classes.Theme}>
        <Button
          variant="contained"
          color="primary"
          startIcon={darkMode ? <Brightness7Icon /> : <Brightness4Icon />}
          onClick={handleThemeToggle}
        >
          {darkMode ? "Light Mode" : "Dark Mode"}
        </Button>
      </div>
    </div>
  );
};

export default Navbar;
