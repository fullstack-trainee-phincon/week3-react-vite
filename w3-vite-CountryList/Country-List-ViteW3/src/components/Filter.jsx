import React from "react";
import { FormControl, InputLabel, MenuItem, Select } from "@mui/material";

const Filter = ({ selectedRegion, handleFilterChange }) => {
  return (
    <FormControl sx={{ minWidth: 200, marginTop: 5 }}>
      <InputLabel id="region-filter-label">Filter by Region</InputLabel>
      <Select
        labelId="region-filter-label"
        id="region-filter-select"
        value={selectedRegion}
        label="Filter by Region"
        onChange={handleFilterChange}
      >
        <MenuItem value="All">All</MenuItem>
        <MenuItem value="Africa">Africa</MenuItem>
        <MenuItem value="Americas">America</MenuItem>
        <MenuItem value="Asia">Asia</MenuItem>
        <MenuItem value="Europe">Europe</MenuItem>
        <MenuItem value="Oceania">Oceania</MenuItem>
      </Select>
    </FormControl>
  );
};

export default Filter;
