import "./App.css";
import FooterBackground from "./assets/images/pattern-hills.svg";
import Facebook from "./assets/images/icon-facebook.svg";
import Pinterest from "./assets/images/icon-pinterest.svg";
import Instagram from "./assets/images/icon-instagram.svg";
import React from "react";
import ReactDOM from "react-dom";
import Countdown from "react-countdown";

const CountdownTimers = ({ days, hours, minutes, seconds }) => {
  const timers = [
    { label: "Days", value: days },
    { label: "Hours", value: hours },
    { label: "Minutes", value: minutes },
    { label: "Seconds", value: seconds },
  ];

  return (
    <div className="Countdown-Box">
      {timers.map((timer, index) => (
        <div className="Countdown-Timer" key={index}>
          <div className="CountdownUp" />
          <div className="CountdownDown" />
          <div className="Timer">{timer.value}</div>
          <div className="Label">{timer.label}</div>
        </div>
      ))}
    </div>
  );
};

const App = () => {
  const date = Date.now() + 365 * 24 * 60 * 60 * 1000;

  const renderer = ({ days, hours, minutes, seconds }) => {
    return (
      <CountdownTimers
        days={days}
        hours={hours}
        minutes={minutes}
        seconds={seconds}
      />
    );
  };

  return (
    <div className="Container-Main">
      <div className="Text1">We are launching soon</div>
      <Countdown date={date} renderer={renderer} />
      <div className="Footer">
        <div className="Social-Footer">
          <img src={Facebook} alt="Facebook" />
          <img src={Pinterest} alt="Pinterest" />
          <img src={Instagram} alt="Instagram" />
        </div>
      </div>
    </div>
  );
};

export default App;
